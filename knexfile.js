// Update with your config settings.

module.exports = {
	development: {
		client: 'mysql',
		connection: {
			host: 'localhost',
			user: 'root',
			password: 'Projects0ft20!9',
			database: 'insurance_lookup'
		},
		migrations: {
			directory: __dirname + '/db/migrations'
		},
		seeds: {
			directory: __dirname + '/db/seeds'
		}
	},
	// development: {
	// 	client: 'mysql',
	// 	connection: {
	// 		host: 'localhost',
	// 		user: 'admin',
	// 		password: '1234',
	// 		database: 'insurance_lookup'
	// 	},
	// 	migrations: {
	// 		directory: __dirname + '/db/migrations'
	// 	},
	// 	seeds: {
	// 		directory: __dirname + '/db/seeds'
	// 	}
	// },
	production: {
		client: 'mysql',
		connection: {
			host: 'localhost',
			user: 'root',
			password: 'Projects0ft20!9',
			database: 'insurance_lookup'
		},
		migrations: {
			directory: __dirname + '/db/migrations'
		},
		seeds: {
			directory: __dirname + '/db/seeds'
		}
	}
};
