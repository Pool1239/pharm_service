/* สําหรับสร้าง Controller */

const { success, failed } = require('../../config/response');
const { set_cookie, remove_cookie } = require('../../config/cookie_action');
const PharmModel = require('./PharmModel');
const bcrypt = require('bcrypt');

class AuthController {
	async allUser(req, res) {
		try {
			let data = await PharmModel.allUser();
			let pharma = await PharmModel.Pharmacy();
			let datamap = data.map((e) => {
				let pharmacy = pharma.find((el) => el.pharmacy_id === e.pharmacy_id);
				return {
					pharmacy_id: e.pharmacy_id,
					pharmacist_id: e.pharmacist_id,
					pharmacy_code: e.pharmacy_code,
					pharmacist_name: e.pharmacist_name,
					pharmacist_type: e.pharmacist_type,
					pharmacy: pharmacy,
					pharmacist_image_base64: e.pharmacist_image_base64
						? 'data:image/jpeg;base64,' + e.pharmacist_image_base64.toString('ascii')
						: null
				};
			});
			success(res, datamap);
		} catch (error) {
			failed(res, 'ไม่พบข้อมูลผู้ใช้งาน');
		}
	}
	async getCompany(req, res) {
		try {
			let cpn = await PharmModel.getCompany();
			let cpn_cv = await PharmModel.getCompanyxcoverage();
			let data = cpn.map((e) => {
				let fill = cpn_cv.filter((el) => el.company_code === e.company_code);
				let coverage = fill.map((el) => ({
					company_code: el.company_code,
					coverage_type: el.coverage_type,
					coverage_voucher_flag: el.coverage_voucher_flag,
					coverage_amount_display_threshold: el.coverage_amount_display_threshold,
					coverage_icon_base64: el.coverage_icon_base64
						? 'data:image/jpeg;base64,' + el.coverage_icon_base64.toString('ascii')
						: null
				}));
				return { ...e, coverage };
			});
			success(res, data);
		} catch (error) {
			failed(res, 'ไม่มีบริษัท');
		}
	}
	async Companyxserviceoption(req, res) {
		try {
			let id = req.body.company_code;
			let data = await PharmModel.getCompanyxserviceoption(id);
			success(res, data);
		} catch (error) {
			failed(res, 'ไม่มีประเภทการบริการ');
		}
	}
	async Companyxservice(req, res) {
		try {
			let body = req.body;
			let obj = {
				company_code: body.company_code,
				service_option: body.service_option
			};
			let data = await PharmModel.getCompanyxservice(obj);
			success(res, data);
		} catch (error) {
			failed(res, 'ไม่มีกลุ่มโรคที่เลือก');
		}
	}
	async Medicalitems(req, res) {
		try {
			let data = await PharmModel.Medicalitems();
			success(res, data);
		} catch (error) {
			failed(res, 'ไม่พบรายการยา');
		}
	}
	async Medicine(req, res) {
		try {
			let data = await PharmModel.Medicine();
			success(res, data);
		} catch (error) {
			failed(res, 'ไม่พบประเภทยา');
		}
	}
	async SearchMedicalitems(req, res) {
		try {
			let text = req.body.text;
			let data1 = await PharmModel.SearchMedicalitems(text, 1);
			let data2 = await PharmModel.SearchMedicalitems(text, 0);
			let datamap = data1.map((e) => e.trade_name).concat(data2.map((e) => e.generic_ingredient));
			success(res, datamap);
		} catch (error) {
			failed(res, 'ไม่พบรายการยา');
		}
	}
	async Dosage(req, res) {
		try {
			let data = await PharmModel.Dosage();
			success(res, data);
		} catch (error) {
			failed(res, 'ไม่พบรูปแบบยา');
		}
	}
	async reloadItem(arr) {
		try {
			let trun = await PharmModel.truncateItem('medical_items');
			if (trun[0].protocol41) {
				let arr_item = arr.medicalLookupItems.filter((e) => e.status == 1).map((e) => ({
					tpu_code: e.tpuCode,
					trade_name: e.tradeName,
					generic_ingredient: e.activeIngredient,
					status: e.status
				}));
				let data = await PharmModel.newItem('medical_items', arr_item);
				// console.log('data', data);
			} else console.log('error', trun[0].protocol41);
		} catch (error) {
			console.log('error', error);
		}
	}

	async reloadPharm(arr) {
		try {
			let trun = await PharmModel.truncateItem('pharmacy');
			if (trun[0].protocol41) {
				let arr_item = arr.data.map((e) => ({
					pharmacy_id: e.pharmacyId,
					pharmacy_code: e.pharmacyCode,
					pharmacy_store: e.pharmacyStore,
					pharmacy_vat_percent: e.pharmacyVatPercent,
					pharmacy_province_code: e.pharmacyProvinceCode,
					pharmacy_lat: e.pharmacyLat,
					pharmacy_long: e.pharmacyLong,
					pharmcare_program: e.pharmcareProgram
				}));
				await PharmModel.newItem('pharmacy', arr_item);
			} else console.log('error', trun[0].protocol41);
		} catch (error) {
			console.log('error', error);
		}
	}

	async reloadCompanyXServiceOption(arr) {
		try {
			let trun = await PharmModel.truncateItem('lookup_companyxserviceoption');
			if (trun[0].protocol41) {
				let arr_item = arr.companyXServiceOptions.map((e) => ({
					company_code: e.companyCode,
					service_option: e.serviceOption,
					service_key_order: e.serviceKeyOrder,
					service_option_label: e.serviceOptionLabel,
					service_require_prescription: e.serviceRequirePrescription,
					service_option_cost: e.serviceOptionCost
				}));
				await PharmModel.newItem('lookup_companyxserviceoption', arr_item);
			} else console.log('error', trun[0].protocol41);
		} catch (error) {
			console.log('error', error);
		}
	}

	async reloadCompanyXServices(arr) {
		try {
			let trun = await PharmModel.truncateItem('lookup_companyxservice');
			if (trun[0].protocol41) {
				let arr_item = arr.companyXServices.map((e) => ({
					company_code: e.companyCode,
					service_code: e.serviceCode,
					service_option: e.serviceOption,
					service_detail: e.name,
					service_detail_other_flag: e.serviceDetailOtherFlag
				}));
				await PharmModel.newItem('lookup_companyxservice', arr_item);
			} else console.log('error', trun[0].protocol41);
		} catch (error) {
			console.log('error', error);
		}
	}

	async reloadPharmacyXPharmacist(arr) {
		try {
			let trun = await PharmModel.truncateItem('pharmacyxpharmacist');
			if (trun[0].protocol41) {
				let arr_item = arr.data.map((e) => ({
					pharmacy_id: e.pharmacyId,
					pharmacist_id: e.pharmacistId,
					pharmacy_code: e.pharmacyCode,
					pharmacist_name: e.pharmacistName,
					pharmacist_image_base64: e.pharmacistImageBase64,
					pharmacist_type: e.pharmacistType,
					pharmcare_program: e.pharmcareProgram
				}));
				await PharmModel.newItem('pharmacyxpharmacist', arr_item);
			} else console.log('error', trun[0].protocol41);
		} catch (error) {
			console.log('error', error);
		}
	}

	async reloadCompanyLookup(arr) {
		try {
			let trun = await PharmModel.truncateItem('lookup_company');
			if (trun[0].protocol41) {
				let arr_item = arr.data.map((e) => ({
					company_code: e.companyCode,
					company_name: e.companyName
				}));
				await PharmModel.newItem('lookup_company', arr_item);
			} else console.log('error', trun[0].protocol41);
		} catch (error) {
			console.log('error', error);
		}
	}

	async reloadCompanyXCoverageLookup(arr) {
		try {
			let trun = await PharmModel.truncateItem('lookup_companyxcoverage');
			if (trun[0].protocol41) {
				let arr_item = arr.data.map((e) => ({
					company_code: e.companyCode,
					coverage_type: e.coverageType,
					coverage_voucher_flag: e.coverageVoucherFlag,
					coverage_amount_display_threshold: e.coverageAmountDisplayThreshold,
					coverage_icon_base64: e.coverageIconBase64
				}));
				await PharmModel.newItem('lookup_companyxcoverage', arr_item);
			} else console.log('error', trun[0].protocol41);
		} catch (error) {
			console.log('error', error);
		}
	}

	async reloadPharmacyXMedicalItemLookup(arr) {
		try {
			let trun = await PharmModel.truncateItem('lookup_pharmacyxmedicalitem');
			if (trun[0].protocol41) {
				let arr_item = arr.data.map((e) => ({
					pharmacy_id: e.pharmacyId,
					tpu_code: e.tpuCode,
					quantity_unit: e.quantityUnit,
					quantity_type: e.quantityType,
					net_price: e.netPrice,
					average_net_price_per_unit: e.averageNetPricePerUnit,
					transaction_date: e.transactionDate,
					claim_internal_id: e.claimInternalId
				}));
				await PharmModel.newItem('lookup_pharmacyxmedicalitem', arr_item);
			} else console.log('error', trun[0].protocol41);
		} catch (error) {
			console.log('error', error);
		}
	}

	async PharmacistType(req, res) {
		try {
			let obj = { pharmacy_id: req.body.pharmacy_id, pharmacist_id: req.body.pharmacist_id };
			let data = await PharmModel.getPharmacistType(obj);
			success(res, data);
		} catch (error) {
			failed(res, 'ไม่พบ Pharmacist');
		}
	}

	async LookupCompanyxserviceoption(req, res) {
		try {
			let data = await PharmModel.LookupCompanyxserviceoption();
			success(res, data);
		} catch (error) {
			console.log(error);
			failed(res, 'ไม่พบ company service');
		}
	}

	async LookupCompany(req, res) {
		try {
			let data = await PharmModel.LookupCompany();
			let new_data = [ ...new Map(data.map((item) => [ item['company_name'], item ])).values() ];
			success(res, new_data);
		} catch (error) {
			console.log(error);
			failed(res, 'ไม่พบ company');
		}
	}

	async PharmacyXPharmacist(req, res) {
		try {
			let obj = { pharmacy_id: req.body.pharmacy_id };
			let data = await PharmModel.getPharmacyXPharmacist(obj);
			success(res, data);
		} catch (error) {
			console.log(error);
			failed(res, 'ไม่พบ pharmacist_name');
		}
	}
	async Pharmacy(req, res) {
		try {
			let data = await PharmModel.Pharmacy();
			success(res, data);
		} catch (error) {
			failed(res, 'ไม่พบ Pharmacy Items');
		}
	}

	async PharmacyXMedicalitem(req, res) {
		try {
			let obj = {
				pharmacy_id: req.body.pharmacyId,
				tpu_code: req.body.tpuCode
			};
			let data = await PharmModel.PharmacyXMedicalitem(obj);
			success(res, data);
		} catch (error) {
			failed(res, 'ไม่พบ PharmacyXMedicalItem');
		}
	}
}

module.exports = new AuthController();
