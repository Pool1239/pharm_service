/* สําหรับเชื่อมต่อ Database */
const knex = require('../../knex');

class AuthModel {
	allUser() {
		return knex('pharmacyxpharmacist');
	}
	getCompany() {
		return knex('lookup_company');
	}
	getCompanyxserviceoption(id) {
		return knex('lookup_companyxserviceoption').where('company_code', id).orderBy('service_key_order', 'ASC');
	}
	getCompanyxservice(data) {
		return knex('lookup_companyxservice').where(data);
	}
	Medicalitems() {
		return knex('medical_items');
	}
	Medicine() {
		return knex('lookup_medicine');
	}
	SearchMedicalitems(text, num) {
		let length = text.length;
		if (length) {
			if (num) {
				return (
					knex('medical_items')
						.distinct('trade_name')
						// .count('trade_name')
						.where('trade_name', 'like', length > 1 ? `%${text}%` : `${text}%`)
						.limit(100)
				);
			} else {
				return (
					knex('medical_items')
						.distinct('generic_ingredient')
						// .count('trade_name')
						.where('generic_ingredient', 'like', length > 1 ? `%${text}%` : `${text}%`)
						.limit(100)
				);
			}
		}
	}
	Dosage() {
		return knex('lookup_dosage_form');
	}
	getCompanyxcoverage() {
		return knex('lookup_companyxcoverage');
	}
	truncateItem(tbl) {
		return knex(tbl).truncate();
	}
	newItem(tbl, arr) {
		return knex(tbl).insert(arr);
	}

	getPharmacistType(obj) {
		return knex('pharmacyxpharmacist').select('pharmacist_type').where(obj);
	}

	LookupCompanyxserviceoption() {
		return knex('lookup_companyxservice').distinct('service_option').select('service_option');
	}

	LookupCompany() {
		return knex('lookup_company').select('company_name', 'company_code');
	}

	getPharmacyXPharmacist(obj) {
		return knex('pharmacyxpharmacist').select('pharmacist_name', 'pharmacist_id').where(obj);
	}
	Pharmacy() {
		return knex('pharmacy');
	}
	Pharmacy_id(id) {
		return knex('pharmacy').where(id);
	}

	PharmacyXMedicalitem(obj){
		if (!obj.pharmacy_id && !obj.tpu_code)
			return knex('lookup_pharmacyxmedicalitem');
		else if(!obj.pharmacy_id && obj.tpu_code){
			return knex('lookup_pharmacyxmedicalitem').whereIn('tpu_code', obj.tpu_code);
		}
		else if(obj.pharmacy_id && !obj.tpu_code){
			return knex('lookup_pharmacyxmedicalitem').where('pharmacy_id', obj.pharmacy_id);
		}else{
			return knex('lookup_pharmacyxmedicalitem').where('pharmacy_id', obj.pharmacy_id).whereIn('tpu_code', obj.tpu_code);
		}
	}
}

module.exports = new AuthModel();
