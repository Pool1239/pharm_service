/* สําหรับเชื่อต่อ Route */
const { Router } = require('express');
const { check } = require('express-validator/check');
const { validator } = require('../../middleware/validator');
const router = Router();

const PharmController = require('./PharmController');

router.get('/alluser', PharmController.allUser);
router.get('/company', PharmController.getCompany);
router.post('/companyxserviceoption', PharmController.Companyxserviceoption);
router.post('/companyxservice', PharmController.Companyxservice);
router.get('/medical_items', PharmController.Medicalitems);
router.get('/medicine', PharmController.Medicine);
router.post('/search_like', PharmController.SearchMedicalitems);
router.get('/dosage', PharmController.Dosage);
router.post('/pharmacist_type', PharmController.PharmacistType);
router.get('/lookup_companyxserviceoption', PharmController.LookupCompanyxserviceoption);
router.get('/lookup_company', PharmController.LookupCompany);
router.post('/pharmacyxpharmacist', PharmController.PharmacyXPharmacist);
router.get('/pharmacy', PharmController.Pharmacy);
router.post('/pharmacyxmedicalitem',PharmController.PharmacyXMedicalitem)
module.exports = router;
