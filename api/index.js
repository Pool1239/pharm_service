const pharm = require('./pharm/PharmRoute');

module.exports = (app, version) => {
	app.use(version + '/pharm', pharm);
};
