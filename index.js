const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const cookieSession = require('cookie-session');
const logger = require('morgan');
const path = require('path');
const fs = require('fs');
const moment = require('moment-timezone');
const date = moment().format('DD-MM-YYYY');
const time = moment().format('HH: mm: ss');
const app = express();

const version = '/api/v1';
const port = process.env.PORT || 3099;
const schdule = require('./src/schedule')


// cors
const whitelist = [
	'http://35.244.108.206:3000',
	'https://35.244.108.206:3000',
	'http://172.16.63.199:3000',
	'http://172.16.63.41:3000',
	'http://localhost:3000',
	'http://35.244.108.206',
	'https://healthcare-uat.pharmcare.co',
	undefined,
	'chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop'
];
const corsOption = {
	origin: (origin, cb) => {
		console.log('origin ', origin);
		if (whitelist.indexOf(origin) !== -1) {
			cb(null, true);
		} else {
			cb(new Error('Not allowed by CORS'));
		}
	},
	optionsSuccessStatus: 200,
	preflightContinue: true,
	credentials: true
};
app.use(cors(corsOption));

// set body parser
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(bodyParser.json({ limit: '50mb' }));

// set cookie
app.enable('trust proxy');
app.use(
	cookieSession({
		name: '_cf',
		keys: [ 'eiei' ],
		maxAge: 8 * 30 * 24 * 60 * 60 * 1000,
		secure: false,
		httpOnly: true
	})
);

// serve static file
app.use(version + '/static', express.static(path.join(__dirname, 'public')));

//port
app.listen(port, () => console.log('listenning at port ', port));

// logs
app.use(logger('dev'));
var accessLogStream = fs.createWriteStream(`${__dirname}/logs/${date}.log`, {
	flags: 'a'
});
var configlog = `[${time}] [ip]: :remote-addr :remote-user [method]: :method [url]: :url HTTP/:http-version [status]: :status [response-time]: :response-time ms [client]: :user-agent`;
app.use(logger(configlog, { stream: accessLogStream }));

const createApi = require('./api/index');
createApi(app, version);
