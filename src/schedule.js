const fetch = require('node-fetch');
const Bluebird = require('bluebird');
const cron = require('node-cron');
const PharmController = require('../api/pharm/PharmController');
const moment = require('moment-timezone');
fetch.Promise = Bluebird;

//schedule

const POST = (path, obj) => {
	return new Promise((resolve, reject) => {
		fetch('https://api-uat.pharmcare.co' + path, {
			method: 'post',
			body: JSON.stringify(obj),
			headers: { 'Content-Type': 'application/json', 'x-api-key': 'PHARMCARE-00' }
		})
			.then((res) => res.json())
			.then((json) => (json.statusCode >= 400 ? reject(json) : resolve(json)));
	});
};

cron.schedule('0 0 1 * * *', async () => {
	try {
		let objMed = {
			beginTimestamp: null,
			endTimestamp: moment().add(1, 'days').format('YYYY-MM-DD.00.00.00')
		};

		let medical = await POST('/medicalLookupItem', objMed);
		if (medical.statusCode <= 299) {
			console.log('medical');
			await PharmController.reloadItem(medical);
		}
		console.log('tesssss');
	} catch (error) {
		console.log(error);
		// console.log(`fail http://34.93.109.176:8080/medicalLookupItem ,Date = ${date} ,Time = ${time} `);
	}
});

cron.schedule('20 0 1 * * *', async () => {
	try {
		let objPham = {
			healthcareProviderId: null,
			pharmacyId: null
		};
		let pharm = await POST('/pharmacyLookup', objPham);
		if (pharm.statusCode <= 299) {
			console.log('pharmacyLookup');
			await PharmController.reloadPharm(pharm);
		}
		console.log('tesssss2');
	} catch (error) {
		console.log(error);
		// console.log(`fail http://34.93.109.176:8080/medicalLookupItem ,Date = ${date} ,Time = ${time} `);
	}
});

cron.schedule('40 0 1 * * *', async () => {
	try {
		let objCompany = {
			companyCode: '00'
		};
		let companyXService = await POST('/companyXService', objCompany);
		console.log('companyXService');
		if (companyXService.statusCode <= 299) {
			await PharmController.reloadCompanyXServiceOption(companyXService);
			await PharmController.reloadCompanyXServices(companyXService);
		}
		console.log('tesssss3');
	} catch (error) {
		console.log(error);
		// console.log(`fail http://34.93.109.176:8080/medicalLookupItem ,Date = ${date} ,Time = ${time} `);
	}
});

cron.schedule('0 1 1 * * *', async () => {
	try {
		let objPharmacist = {
			healthcareProviderId: null,
			pharmacyId: null
		};
		let pharmacyXPharmacistLookup = await POST('/pharmacyXPharmacistLookup', objPharmacist);
		console.log('pharmacyXPharmacistLookup');
		if (pharmacyXPharmacistLookup.statusCode <= 299) {
			await PharmController.reloadPharmacyXPharmacist(pharmacyXPharmacistLookup);
			// await PharmController.reloadCompanyXServices(companyXService)
		}
		console.log('tesssss4');
	} catch (error) {
		console.log(error);
		// console.log(`fail http://34.93.109.176:8080/medicalLookupItem ,Date = ${date} ,Time = ${time} `);
	}
});

cron.schedule('20 1 1 * * *', async () => {
	try {
		let objCompanyLookup = {
			companyCode: null
		};
		let companyLookup = await POST('/companyLookup', objCompanyLookup);
		console.log('companyLookup');
		if (companyLookup.statusCode <= 299) {
			await PharmController.reloadCompanyLookup(companyLookup);
			// await PharmController.reloadCompanyXServices(companyXService)
		}
		console.log('tesssss5');
	} catch (error) {
		console.log(error);
		// console.log(`fail http://34.93.109.176:8080/medicalLookupItem ,Date = ${date} ,Time = ${time} `);
	}
});

cron.schedule('40 1 1 * * *', async () => {
	try {
		let objCompanyXCoverage = {
			companyCode: null
		};
		let companyXCoverageLookup = await POST('/companyXCoverageLookup', objCompanyXCoverage);
		console.log('companyXCoverageLookup');
		if (companyXCoverageLookup.statusCode <= 299) {
			await PharmController.reloadCompanyXCoverageLookup(companyXCoverageLookup);
			// await PharmController.reloadCompanyXServices(companyXService)
		}
		console.log('tesssss6');
	} catch (error) {
		console.log(error);
		// console.log(`fail http://34.93.109.176:8080/medicalLookupItem ,Date = ${date} ,Time = ${time} `);
	}
});

cron.schedule('0 2 1 * * *', async () => {
	try {
		let objPharmacyXMedicalItemLookup = {
			companyCode: null
		};
		let pharmacyXMedicalItemLookup = await POST('/pharmacyXMedicalItemLookup', objPharmacyXMedicalItemLookup);
		console.log('pharmacyXMedicalItemLookup');
		if (pharmacyXMedicalItemLookup.statusCode <= 299) {
			await PharmController.reloadPharmacyXMedicalItemLookup(pharmacyXMedicalItemLookup);
			// await PharmController.reloadCompanyXServices(companyXService)
		}
		console.log('tesssss6');
	} catch (error) {
		console.log(error);
		// console.log(`fail http://34.93.109.176:8080/medicalLookupItem ,Date = ${date} ,Time = ${time} `);
	}
});
